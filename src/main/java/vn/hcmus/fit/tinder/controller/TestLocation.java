package vn.hcmus.fit.tinder.controller;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.hcmus.fit.tinder.projection.ILocation;
import vn.hcmus.fit.tinder.repository.MarkerRepository;
import vn.hcmus.fit.tinder.util.ReponseUtils;

/**
 * Created by Asus on 12/28/2019.
 */
@RestController
public class TestLocation {
    @Autowired
    MarkerRepository markerRepository;

    @PersistenceContext
    public EntityManager entityManager;

    @GetMapping(value = "/find",produces = "application/json")
    public String findNearlyLocationToDating(@RequestParam(name = "lat") double lat,
                                             @RequestParam(name = "lng") double lng,
                                             @RequestParam(name = "page") int page,
                                             @RequestParam(name = "limit") int limit){
//
//        Pageable p = PageRequest.of(page,limit);
        List<ILocation> res = markerRepository.findNearlyLocation(lat,lng,page-1,limit);
//        int lat = (int)(Math.random() * ((90 + 90) + 1)) -90;
//        int lan = (int)(Math.random() * ((180 + 180) + 1)) -180;


//        Session session = entityManager.unwrap(Session.class);
//        org.hibernate.Query hibernateQuery = session.createNativeQuery(
//                "SELECT m.id ,m.lat, m.lng,(3959 * acos(cos(radians(:lat)) * cos(radians(m.lat)) * cos(radians(m.lng) - radians(:lng)) + sin(radians(:lat)) * sin(radians(m.lat)))) AS distance FROM MARKERS m HAVING distance > 25 ORDER BY distance")
//                .setParameter("lat",lat).setParameter("lng",lng);
//
//        hibernateQuery.setFirstResult(page);
//        hibernateQuery.setMaxResults(limit);
//
//
//        hibernateQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
//        List<Map<String, Object>> res = hibernateQuery.list();
//        List<Object[]> res = jpaQuery.getResultList();
////        res.forEach(System.out::println);
//        System.out.println(res.get(0).getLat());
        return ReponseUtils.returnListLocation(res).toString();
    }
}
