package vn.hcmus.fit.tinder.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.hcmus.fit.tinder.entity.Marker;
import vn.hcmus.fit.tinder.projection.ILocation;

import java.util.List;


/**
 * Created by Asus on 12/28/2019.
 */
@Repository
public interface MarkerRepository extends JpaRepository<Marker,Long> {
    @Query(
            value = "SELECT m.lat as lat, m.lng as lng,(3959 * acos(cos(radians(:lat)) * cos(radians(m.lat)) * cos(radians(m.lng) - radians(:lng)) + sin(radians(:lat)) * sin(radians(m.lat)))) as distance  FROM MARKERS m HAVING distance > 25 ORDER BY distance LIMIT :page, :limit",
            nativeQuery = true)
//    Page<ILocation> findNearlyLocation(@Param("lat") double lat, @Param("lng") double lng, Pageable pageable);
    List<ILocation> findNearlyLocation(@Param("lat") double lat, @Param("lng") double lng, @Param("page") int page, @Param("limit") int limit);
}
