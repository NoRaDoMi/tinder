package vn.hcmus.fit.tinder.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Asus on 12/28/2019.
 */
@Data
@Entity
@Table(name = "markers") // Danh sach truyen
public class Marker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private Double lat;

    @Column(unique = true)
    private Double lng;
}
