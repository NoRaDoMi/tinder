package vn.hcmus.fit.tinder.projection;

/**
 * Created by Asus on 1/11/2020.
 */
public interface ILocation {
//    private double lat;
//    private  double lng;
//    private double dist;
//
//    public ILocation(double lat, double lng, double dist) {
//        this.lat = lat;
//        this.lng = lng;
//        this.dist = dist;
//    }
//
//    public Double getLat() {
//        return lat;
//    }
//
//    public void setLat(Double lat) {
//        this.lat = lat;
//    }
//
//    public Double getLng() {
//        return lng;
//    }
//
//    public void setLng(Double lng) {
//        this.lng = lng;
//    }
//
//    public Double getDist() {
//        return dist;
//    }
//
//    public void setDist(Double dist) {
//        this.dist = dist;
//    }
    Double getLat();
    Double getLng();
    Double getDistance();
}
