package vn.hcmus.fit.tinder.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.data.domain.Page;
import vn.hcmus.fit.tinder.projection.ILocation;

import java.util.List;
import java.util.Locale;

/**
 * Created by Asus on 1/11/2020.
 */
public class ReponseUtils {
    private static ObjectMapper mapper = new ObjectMapper();

    public static ObjectNode   returnLocation(ILocation location){
        ObjectNode node = mapper.createObjectNode();
        node.put("kinh do",location.getLat());
        node.put("vi do",location.getLng());
        node.put("khoang cach",location.getDistance());
        return node;
    }

    public static ArrayNode returnListLocation(List<ILocation> locationList){
        ArrayNode nodes = mapper.createArrayNode();
        for (ILocation l : locationList) {
            nodes.add((returnLocation(l)));
        }
        return nodes;
    }


    public static String successPagination(Page<ILocation> p){
        ObjectNode node = mapper.createObjectNode();

        ObjectNode body = mapper.createObjectNode();
        body.put("page",p.getNumber()+1);
        body.put("total_results",p.getTotalElements());
        body.put("total_pages",p.getTotalPages());
        body.set("results",returnListLocation(p.getContent()));
        node.set("Response",body);
        return node.toString();
    }
}
